﻿using System;

namespace ShiftArrayRight
{
    class Program
    {
        public static void Main(string[] args)
        {
            // integar array
            int[] intArray = { 1, 2, 3, 4, 5 };

            // array size
            int intArrayLength = intArray.Length;

            // number of indexes to shift integar to the right in the array
            int shift = 1;

            // call method ShiftRight
            ShiftRight(intArray, intArrayLength, shift);
        }

        public static void ShiftRight(int[] oldArray, int oldArrayLength, int n)
        {
            // initialize a new array with length of old array
            var newArray = new int[oldArrayLength];

            // loop through old array
            for (var i = 0; i < oldArrayLength; i++)
            {
                var a = i + n;
                var N = newArray.Length;

                // a % N
                // a = a - (a / N) * N
                // a / N = b.bb; b * N = q; a - q = r
                // 123 % 4 = 3
                // 123 / 4 = 30.75; 30 * 4 = 120; 123 - 120 = 3
                // 1 % 5 = 1
                // 2 % 5 = 2
                // 3 % 5 = 3
                // 4 % 5 = 4
                // 5 % 5 = 0
                // 6 % 5 = 1
                // 7 % 5 = 2
                // 8 % 5 = 3
                // 9 % 5 = 4
                // 10 % 5 = 0
                var remainder = a % N;

                newArray[remainder] = oldArray[i];
            }

            // Result: Old array: 12345
            Console.Write("Old array: ");
            foreach (var num in oldArray)
            {
                Console.Write(num);
            }

            // Result: Shifted array: 51234
            Console.Write("\nShifted array: ");
            foreach (var num in newArray)
            {
                Console.Write(num);
            }
        }
    }
}
